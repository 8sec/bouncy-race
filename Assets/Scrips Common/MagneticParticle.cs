﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

/*
This scrips have to be put on particle system.
Then you can use this script to change particle position and have a magnetic effect to move your particle to your player for example

Autor = Théo
*/
public class MagneticParticle : MonoBehaviour
{
    [Title("Setting")]
    public bool shrinkSize = true;
    [Space]
    public float distanceForCollect = 0.1f;
    public float delayBeforeMagnet = 1f;
    public float durationLerp = 1f;

    public bool debug = false;
    [ShowIf("debug")] [ReadOnly] [SerializeField] private float lerpValueMagnet = 0;
    [ShowIf("debug")] [ReadOnly] [SerializeField] private float cooldownValue = 0;
    
    private float startSize;
    private Vector3[] ParticleStartPosition;
    private bool GetStartValue = false;
    private new ParticleSystem particleSystem;
    private ParticleSystem.Particle[] particles;

    // Start is called before the first frame update
    void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

    private void GetStartValueParticle(){
        startSize = particleSystem.startSize;

        InitializeIfNeeded();
        int particleAlive = particleSystem.GetParticles(particles);
        ParticleStartPosition = new Vector3[particleAlive];

        for (int i = 0; i < particleAlive; i++)
        {
            ParticleStartPosition[i] = particles[i].position;
        }
        GetStartValue = true;

    }

    private void LateUpdate() {
        if(particleSystem.isPlaying){
            cooldownValue += Time.deltaTime;

            if(cooldownValue >= delayBeforeMagnet && lerpValueMagnet < 1){
                    if(GetStartValue == false)
                        GetStartValueParticle();

                    InitializeIfNeeded();
                    lerpValueMagnet += Time.deltaTime / durationLerp;


                    int particlesAlive = particleSystem.GetParticles(particles);
                    Vector3 playerPosition = transform.position; // Change this to get player position

                    for (int i = 0; i < particlesAlive; i++)
                    {
                        particles[i].position = Vector3.Lerp(ParticleStartPosition[i], playerPosition, lerpValueMagnet);
                        if(shrinkSize)
                            particles[i].startSize = Mathf.Lerp(startSize, startSize / 2, lerpValueMagnet);

                        if(Vector3.Distance(particles[i].position, playerPosition) < distanceForCollect){
                            particles[i].startSize = 0f;
                        }
                    }

                    particleSystem.SetParticles(particles, particlesAlive);
            }
        }else{
            lerpValueMagnet = 0;
            cooldownValue = 0;
            particleSystem.Stop();
        }
    }


    void InitializeIfNeeded()
    {
        if (particleSystem == null)
            particleSystem = GetComponent<ParticleSystem>();

        if (particles == null || particles.Length < particleSystem.main.maxParticles)
            particles = new ParticleSystem.Particle[particleSystem.main.maxParticles];
    }

}
