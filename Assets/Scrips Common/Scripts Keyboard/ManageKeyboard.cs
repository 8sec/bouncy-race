﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageKeyboard : MonoBehaviour
{
    public static ManageKeyboard instance;

    public List<LetterUI> listLetter = new List<LetterUI>();
    private Dictionary<string, LetterUI> dicoLetter = new Dictionary<string, LetterUI>();

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        for (int i = 0; i < listLetter.Count; i++)
        {
            dicoLetter.Add(listLetter[i].letter.ToUpper(), listLetter[i]);
        }
    }

    public void letterPress(string letter,bool rightLetter = true)
    {
        letter = letter.ToUpper();

        LetterUI tmpUI;
        if (dicoLetter.TryGetValue(letter,out tmpUI))
        {
            tmpUI.Press(rightLetter);
        }
        else
        {
            Debug.LogError("Letter doesn't exsit :: " + letter);
        }
    }
}
