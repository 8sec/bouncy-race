﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extender
{ 

    public static Vector3 SetX(this Vector3 v, float value)
    {
        return new Vector3(value, v.y, v.z);
    }

    public static Vector3 SetY(this Vector3 v, float value)
    {
        return new Vector3(v.x, value, v.z);
    }

    public static Vector3 SetZ(this Vector3 v, float value)
    {
        return new Vector3(v.x, v.y, value);
    }

    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}
