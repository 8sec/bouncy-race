﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using GameAnalyticsSDK;
using UnityEngine;
public class SocialManager : MonoBehaviour {

    public static SocialManager Instance;

    void Awake ()
    {

        if (Instance != null)
        {
            Destroy(gameObject);
        }
         
        else
        {
            Instance = this;
            GameAnalytics.Initialize();
            DontDestroyOnLoad(gameObject);

            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                // Already initialized, signal an app activation App Event
                FB.ActivateApp();
            }
        }
        
    }

    private void InitCallback () {
        if (FB.IsInitialized) {
            // Signal an app activation App Event
            FB.ActivateApp ();
            // Continue with Facebook SDK
            // ...
        } else {
            Debug.Log ("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity (bool isGameShown) {
        if (!isGameShown) {
            Debug.LogError ("Paused game");
            // Pause the game - we will need to hide
            Time.timeScale = 0f;
        } else {
            Debug.LogError ("Resumed game");

            // Resume the game - we're getting focus again
            Time.timeScale = 1f;
        }
    }
}