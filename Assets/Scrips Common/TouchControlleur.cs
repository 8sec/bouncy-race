﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using DG.Tweening;
using Lean.Touch;

public class TouchControlleur : MonoBehaviourSingleton<TouchControlleur>
{
    [Title("Settings")]
    // Touch Control
    public float deathZone = 0f;
    public float strenghRotation = 1f;

    private float lastFrameX = 0f;
    private float lastFrameY = 0f;
    private float deltaX = 0f;
    private float deltaY = 0f;
    Vector2 deltaMove = Vector2.zero;
    Vector2 curpos = Vector2.zero;


    [Title("Lean Touch Variable")]
    public float LeanTouchstrenghSensibility = 1f;
    public LeanFingerFilter Use = new LeanFingerFilter(true);

    private void ResetValue()
    {
        lastFrameX = 0f;
        lastFrameY = 0f;
        deltaX = 0f;
        deltaY = 0f;
        deltaMove = Vector2.zero;
    }

    public Vector2 GetDirection()
    {
        if (Input.GetMouseButton(0))
        {
            if (lastFrameX == 0f)
            {
                lastFrameX = Input.mousePosition.x;
                lastFrameY = Input.mousePosition.y;
                curpos = Input.mousePosition;
                deltaX = 0f;
                deltaY = 0f;
                deltaMove = Vector2.zero;
            }

            curpos = Input.mousePosition;

            deltaX = (curpos.x - lastFrameX) * (1242 / Screen.width);
            lastFrameX = curpos.x;

            deltaY = (curpos.y - lastFrameY) * (2688 / Screen.height);
            lastFrameY = curpos.y;

            deltaMove = new Vector2(deltaX, deltaY) * strenghRotation;

            if (deltaMove.magnitude > deathZone)
            {
                return deltaMove;
            }
        }else{
            ResetValue();
        }
        return Vector2.zero;

    }

    public Vector2 GetDirectionNormalized(){
        return GetDirection().normalized;
    }

    public float GetXAxis(){
        return GetDirection().x;
    }

    public float GetYAxis(){
        return GetDirection().y;
    }


    public Vector2 GetInputFromLeanTouch(){
        var fingers = Use.GetFingers();
        Vector2 screenDelta = LeanGesture.GetScaledDelta(fingers);

        return screenDelta * LeanTouchstrenghSensibility;
    }

}
