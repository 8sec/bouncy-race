﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

public class ArtificialIntelligence : PlayerMovement
{
    [Space(20f)]
    [Title("Artificial Inteligence")]
    [ReadOnly] public int percentOfChanceToUseTrampoline = 10;
    [ReadOnly] public int percentOfChanceToMissJump = 5;

    [Title("Rubber Banding")]
    public GameObject character;
    [Space(10f)]
    [Space(10f)] [TabGroup("Behind Player")] [SerializeField] private float behindDistance = -5f;
    [Space(10f)] [TabGroup("Behind Player")] [PropertyRange(0f, 100f)] public int percentOfChanceToMissJumpWhenBehingPlayer = 10;
    [Space(10f)] [TabGroup("Behind Player")] [PropertyRange(0f, 100f)] public int percentOfChanceToUseTrampolineWhenBehingPlayer = 10;
    [Space(10f)] [TabGroup("Behind Player")]  public float durationMovementBehingPlayer = 10;

    [Space(10f)] [TabGroup("CloseTo Player")] [PropertyRange(0f, 100f)] public int percentOfChanceToMissJumpWhenCloseToPlayer = 10;
    [Space(10f)] [TabGroup("CloseTo Player")] [PropertyRange(0f, 100f)] public int percentOfChanceToUseTrampolineWhenCloseToPlayer = 10;
    [Space(10f)] [TabGroup("CloseTo Player")]  public float durationMovementCloseToPlayer = 10;

    [Space(10f)] [TabGroup("In Front of Player")] [SerializeField] private float inFrontDistance = 6f;
    [Space(10f)] [TabGroup("In Front of Player")] [PropertyRange(0f, 100f)] public int percentOfChanceToMissJumpWhenInFrontOfPlayer = 10;
    [Space(10f)] [TabGroup("In Front of Player")] [PropertyRange(0f, 100f)] public int percentOfChanceToUseTrampolineJumpWhenInFrontOfPlayer = 10;

    [Space(10f)] [TabGroup("In Front of Player")] public float durationMovementInFrontOfPlayer = 10;


    public override void Awake() {
        base.Awake();

    }

    public override void Start() {
        base.Start();
        InitIA();
    }

    public void InitIA(){
        ChangeCharacter();
        SetStartPosition();

        ragdollControlleur = GetComponentInChildren<RagdollControlleur>();
        ragdollControlleur.ToogleRagdool(false);
    }

    public override void StartMoving()
    {
        base.StartMoving();
        DetectNextPlatform();
    }

    private void SetStartPosition(){
        List<Platforme> startPlatforms = new List<Platforme>();

        // Ne récupere que les plaformes qui sont sur la ligne actuelle
        for (int i = 0; i < allPlatform.Count; i++)
        {
            if((Mathf.Round(allPlatform[i].transform.position.z) == Mathf.Round(transform.position.z)) && allPlatform[i].isTrampoline == false){
                startPlatforms.Add(allPlatform[i]);
            }    
        }

        int rand = Random.Range(0, startPlatforms.Count);

        transform.position = transform.position.SetX(startPlatforms[rand].transform.position.x);
    }

    private void ChangeCharacter(){
        GameObject newCharacter = ThemeManager.Instance.GetRandomCharacter();
        GameObject instanceCharacter = Instantiate(newCharacter, Vector3.zero, Quaternion.identity, pogoStick);
        instanceCharacter.transform.localPosition = character.transform.localPosition;
        DestroyImmediate(character);
        character = instanceCharacter;
    }

    public override void DetectNextPlatform(){

        List<Platforme> availablePlatforme = new List<Platforme>();

        // Ne récupere que les plaformes qui sont sur la ligne suivante
        for (int i = 0; i < allPlatform.Count; i++)
        {
            bool platformIsOnTheNextRow = (Mathf.Round(allPlatform[i].transform.position.z) == Mathf.Round(transform.position.z + speedForward));
            if(platformIsOnTheNextRow && allPlatform[i].isDestroy == false){
                availablePlatforme.Add(allPlatform[i]);
            }    
        }
        
        // Choisis la platforme la plus proche de l'IA
        Platforme nextPlatform = GetCloserPlatforme(availablePlatforme);


        // Si la platforme est un trampoline
        if(nextPlatform.isTrampoline){
            int rand = Random.Range(0, 100);
            if(rand <= percentOfChanceToUseTrampoline){
                // Utiliser le tranpoline
            }else{
                availablePlatforme.Remove(nextPlatform);
                nextPlatform = GetCloserPlatforme(availablePlatforme);
            }
        }

        MoveIAToNextPlatform(nextPlatform.transform);   

    }

    

    public override void CheckIfGronded()
    {
        base.CheckIfGronded();
        DetectNextPlatform();
    }

    public void MoveIAToNextPlatform(Transform platform){
        float offsetRelativeToPlatform = (platform.lossyScale.x / 2f); // Divise la taille de platform par 2 pour avoir la rayon. 2.2 pour ne pas être au extémité.
        float offsetMovement = 0f;

        // Change les stats de l'IA en fonction de sa position par rapport au joueurs
        RubberBandingMissJump();

        int rand = Random.Range(0, 100);
        if(rand >= percentOfChanceToMissJump){
            // Normal Jump
            offsetMovement = Random.Range(- offsetRelativeToPlatform, offsetRelativeToPlatform);
        }else{
            //Fail Jump
            offsetMovement = offsetRelativeToPlatform + 1f;
            // DebugPlus.LogOnScreen("Fail jump for " + transform.name).Color(Color.red).Duration(5f);                
        }

        if(canMove){
            transform.DOMoveX(platform.position.x + offsetMovement, durationMovement / 1.1f);
        }
    }

    private void RubberBandingMissJump(){
        float distanceFromPlayer = DistanceFromPlayer();
        

        if(distanceFromPlayer < behindDistance){
            percentOfChanceToMissJump = percentOfChanceToMissJumpWhenBehingPlayer;
            percentOfChanceToUseTrampoline = percentOfChanceToUseTrampolineWhenBehingPlayer;

            if(isSuperJump == false)
                durationMovement = durationMovementBehingPlayer;
        }

        if(distanceFromPlayer > inFrontDistance){
            percentOfChanceToMissJump = percentOfChanceToMissJumpWhenInFrontOfPlayer;
            percentOfChanceToUseTrampoline = percentOfChanceToUseTrampolineJumpWhenInFrontOfPlayer;

            if(isSuperJump == false)
                durationMovement = durationMovementInFrontOfPlayer;
        }

        if(MathFunction.IsBetween(distanceFromPlayer, behindDistance, inFrontDistance)){
            percentOfChanceToMissJump = percentOfChanceToMissJumpWhenCloseToPlayer;
            percentOfChanceToUseTrampoline = percentOfChanceToMissJumpWhenCloseToPlayer;

            if(isSuperJump == false)
                durationMovement = durationMovementCloseToPlayer;
        }
    }

    
    public override void Win(int multiplicateurValue){
        canMove = false;
    }

    public override void Lose(){
        canMove = false;
    }


    public override void Update(){
        DisplayRacePosition();
    }

    private float DistanceFromPlayer(){
        // if Z is negative, then the IA is behind the player
        // if Z is possive, then the IA is in front of the player

        return Mathf.Round(transform.position.z - GameControlleur.Instance.playerMovement.transform.position.z);
    }

}
