﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class GameControlleur : MonoBehaviourSingleton<GameControlleur>
{
    public PlayerMovement playerMovement;
    private ArtificialIntelligence[] allIAs;
    private MovingPlatform[] allMovingPlatform;
    public List<GameObject> runners;
    public Material perfectSpotMaterial;

    private bool gameStart = false;
    private bool runnerSetup = false;
    FinishLine finishLine;

    public static int currentGoldPlayer;

    [Title("Perfect Spot")]
    public float durationBeforeFadeOutPerfectSpot = 2f;
    private float tmp_durationFadeOut = 0;
    private bool aphaFadeOut = true;

    [Title("Reference")]
    public Text goldValueTitleScreenText;
    public Text goldValueWinScreenText;
    public Text goldToCollectText;

    public RoadMap roadMap;
    public Image sliderProgression;
    public Leaderboard leaderboard_win;
    public Leaderboard leaderboard_lose;
    public Image fireModeVignette;
    private int goldToAdd = 0;


    [Title("Custom rules of the game")]
    public bool destructivePlatform = false;
    public bool rescuePlayerWhenTheyFall = true;

    // Start is called before the first frame update

    private void Awake() {
        LevelManager.Instance.LoadLevel();
        allIAs = FindObjectsOfType<ArtificialIntelligence>();
        allMovingPlatform = FindObjectsOfType<MovingPlatform>();

        foreach (Platforme platforme in GetAllPlatform())
        {
            platforme.InitPlatform();
        }

        finishLine = FindObjectOfType<FinishLine>();
        SetupRunner();

        currentGoldPlayer = PlayerPrefs.GetInt("goldPlayer", 0);
        
    }

    public void AddGold(int goldToAdd){
        currentGoldPlayer += goldToAdd;
        PlayerPrefs.SetInt("goldPlayer", currentGoldPlayer);
    }

    public FinishLine GetFinishLine(){
        return finishLine;
    }

    private void Start() {
        ThemeManager.Instance.ApplyTheme();
        MonetizationManager.Instance.ShowInterstitial();
        goldValueTitleScreenText.text = currentGoldPlayer.ToString();
        roadMap.UpdateRoadMap();
        UIController.Instance.ShowPanelInstantly(UIPanelName.TITLE_SCREEN);
        FadeOutAlphaSpot(true);
    }

    private void SetRaceProgression(){
        sliderProgression.fillAmount = Mathf.InverseLerp(playerMovement.GetStartYPosition(), finishLine.transform.position.z, playerMovement.GetZPositon());
    }

    private void SetupRunner(){
        runners.Clear();
        if(playerMovement){
            runners.Add(playerMovement.gameObject);
        }

        foreach (ArtificialIntelligence IA in allIAs)
        {
            runners.Add(IA.gameObject);
        }

        runnerSetup = true;
    }
    public void StartGame()
    {
        GameManager.Instance.StartSession();
        if(playerMovement.isActiveAndEnabled){
            playerMovement.StartMoving();
        }

        foreach (MovingPlatform mv in allMovingPlatform)
        {
            mv.StartMovingPlatforme();
        }

        foreach (var IA in allIAs)
        {
            IA.StartMoving();
        }

        gameStart = true;
    }

    public Platforme[] GetAllPlatform(){
        return GameObject.FindObjectsOfType<Platforme>();
    }

    private void Update() {
        if(runnerSetup){
            CalculateRunnerPosition();
            SetRaceProgression();
        }

        tmp_durationFadeOut += Time.deltaTime;

        if(aphaFadeOut == false && tmp_durationFadeOut > durationBeforeFadeOutPerfectSpot){
            FadeOutAlphaSpot(true);
        }
    }

    public PlayerMovement GetPlayer(){
        return playerMovement;
    }

    public void CalculateRunnerPosition(){
        runners.Sort(delegate(GameObject a, GameObject b){
            float distance_a = a.transform.position.z;
            float distance_b = b.transform.position.z;
            if (distance_a==distance_b) return 0;
            if (distance_a >= distance_b)
                return -1;  else  return 1;
        });

        
        for (int i = 0; i < runners.Count; i++)
        {
            runners[i].GetComponent<PlayerMovement>().positionInTheRace = i + 1;
        }
    }


    public void WinGame(int platformMultiplicateur){
        GameAnalyticsSDK.GameAnalytics.NewDesignEvent("RacePositionOfPlayer", playerMovement.positionInTheRace); // Send the race position to GameAnalytics
        finishLine.finishFeedback.PlayFeedbacks();

        int oldGold = currentGoldPlayer;
        goldValueWinScreenText.text = oldGold.ToString();

        if(playerMovement.positionInTheRace == 1){
            goldToAdd = 100 * platformMultiplicateur;
            AddGold(goldToAdd);
            StartCoroutine( leaderboard_win.ShowLeaderboard(1f, true) );
            
        }else{
            goldToAdd = 100 / playerMovement.positionInTheRace;
            AddGold(goldToAdd);
            StartCoroutine( leaderboard_lose.ShowLeaderboard(1f, false) );
        }
        goldToCollectText.text = goldToAdd.ToString();


    }

    public void CollectGold(){
        int newGold = currentGoldPlayer;
        int tmpValueGoldToPrint = newGold - goldToAdd;
        
        DOTween.To(() => tmpValueGoldToPrint, x => tmpValueGoldToPrint = x, newGold, 1.5f).OnUpdate(() =>{
            if(goldValueTitleScreenText){
                goldValueWinScreenText.text = tmpValueGoldToPrint.ToString();
            }
        }).OnComplete(() =>{
            LevelManager.Instance.LoadNextLevel();
        });

    }

    public void LoseGame(){
        StartCoroutine( leaderboard_lose.ShowLeaderboard(1f, false) );
    }

    public ArtificialIntelligence[] GetAllIAs(){
        return allIAs;
    }

    public void ShowVignetteFireMode(bool state){
        switch(state){
            case true:
                fireModeVignette.DOFade(0.5f, 0.5f);
            break;

            case false:
                fireModeVignette.DOFade(0f, 0.5f);
            break;
        }
    }

    public void FadeOutAlphaSpot(bool state){

        switch(state){
            case true:
                perfectSpotMaterial.DOFade(0.4f, 0.5f);
                aphaFadeOut = true;
            break;

            case false:
                perfectSpotMaterial.DOFade(0.8f, 0.1f);
                tmp_durationFadeOut = 0f;
                aphaFadeOut = false;
            break;
        }
    }
}
