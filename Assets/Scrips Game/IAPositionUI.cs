﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IAPositionUI : MonoBehaviour
{
    public Image alive_img;
    public Image dead_img;

    public void ShowStateAlive(bool isAlive){
        if(isAlive){
            alive_img.enabled = true;
            dead_img.enabled = false;
        }else{
            alive_img.enabled = false;
            dead_img.enabled = true;
        }
    }
}
