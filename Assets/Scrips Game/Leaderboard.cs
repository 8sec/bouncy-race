﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaderboard : MonoBehaviour
{
    public List<RowClassement> rowClassement;
    
    public IEnumerator ShowLeaderboard(float delay, bool win){

        yield return new WaitForSeconds(delay);
        
        List<GameObject> allRunner = GameControlleur.Instance.runners;

        for (int i = 0; i < allRunner.Count; i++)
        {
            string effectAnimationForWinner = "incr";
            string effectAnimationForOther = "bounce";
            string effectAnimation;

            if(i == 0){
                effectAnimation = effectAnimationForWinner;
            }else{
                effectAnimation = effectAnimationForOther;
            }

            if(allRunner[i].GetComponent<PlayerMovement>().isPlayer){
                rowClassement[i].pseudo.color = Color.green;
            }else{
                rowClassement[i].pseudo.color = Color.white;
            }
            rowClassement[i].pseudo.text = "<" + effectAnimation + ">" + allRunner[i].GetComponent<PlayerMovement>().pseudo + "</" + effectAnimation + ">";
        }

        if(win){
            GameManager.Instance.LevelSuccess();
        }else{
            GameManager.Instance.GameOver();
        }
    }
}
