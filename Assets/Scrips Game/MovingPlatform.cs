﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

public class MovingPlatform : MonoBehaviour
{
    public bool isMoving = false;

    [Title("Moving Settings")]
    public DOTweenAnimation dOTweenAnimation;


    public void StartMovingPlatforme()
    {
        if(isMoving){
            dOTweenAnimation.DOPlay();
        }
    }

    public void StopMoving(){
        dOTweenAnimation.DOPause();
    }
}
