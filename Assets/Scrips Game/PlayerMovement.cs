﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using TMPro;
using MoreMountains.Feedbacks;
using MoreMountains.NiceVibrations;
using EPOOutline;

public class PlayerMovement : MonoBehaviour
{
    [Title("Settings Jump", "Change normal jump behaviour")]
    [PropertyTooltip("Move *value* unit per seconds")] public float speedForward = 1f;
    private float originalSpeedForwad;
    [PropertyTooltip("Speed of jump movement")] public float durationMovement = 1f;
    private float originalDurationMovement;
    [PropertyTooltip("Speed of jump movement")] public float jumpHeight = 3f;
    private float originaljumpHeight;

    [Title("Settings Super Jump", "Change super jump behaviour")]
    [PropertyTooltip("Move *value* unit per seconds")] public float superSpeedForward = 1f;
    [PropertyTooltip("Speed of jump movement")] public float superDurationMovement = 1f;
    [PropertyTooltip("Speed of jump movement")] public float superJumpHeight = 3f;

    [Title("Settings Giant PowerUp")]
    public float durationGiantPowerUp = 3f;
    public float giantSpeedForward = 1f;
    private float tmpCooldownGiantPowerUp;
    public float giantScale = 2f;
    private Vector3 originalScale;
    public float giantDurationMovement = 1f;
    public float giantJumpheight = 3f;


    [Space]
    [Title("Settings Movement Player")]
    public float speedMovementPlayer = 1f;
    public float speedMovementInput = 1f;
    public float radiusSphereCastDetectionPlatform = 1f;

    [Space]
    [Title("Settings Rotation Player")]
    public float speedRotationInput = 1f;
    public float speedRotationPlayer = 1f;
    public float maxAngle = 30f;
    public float minAngle = -30f;
    public Transform perssonageParent;

    [Space]
    [Title("Fire Mode")]
    public int numberOfChainPerfectSpotToActivateFireMode = 5;
    public int numberOfPlatformToJump = 5;
    public float durationMovementDuringFireMode = 5f;
    public float jumpHeightOutFireMode = 5f;
    public float durationMovementOutFireMode = 5f;

    [SerializeField] [ReadOnly] private int tmp_platformJumpOnFireMode = 0;
    [SerializeField] [ReadOnly] private int numberOfChainPerfectSpot = 0;

    
    [Space]
    [Title("Other Settings", "You should't change that value")]
    public string pseudo;
    public float durationFallInWater = 1f;
    public float forceProjectionWhenKilled = 20f;
    private float originalDurationFallInWater;
    public LayerMask layerPlatform;


    [Title("Control value", "Inspecting game")]
    [SerializeField] [ReadOnly] public bool canMove = false;
    [SerializeField] [ReadOnly] public bool killed = false;
    [SerializeField] [ReadOnly] public bool isSuperJump = false;
    [SerializeField] [ReadOnly] public bool isGiant = false;
    [SerializeField] [ReadOnly] public bool isOnFireMode = false;
    [SerializeField] [ReadOnly] public bool isOnFinisher = false;


    [ReadOnly] public bool isPlayer = true;

    [ReadOnly] public int positionInTheRace = 0;


    [Title("Reference")]
    public List<Platforme> allPlatform;

    public Transform targetPosition;
    public TouchControlleur touchControlleur;
    public TextMeshPro racePositionText;
    public Transform pogoStick;
    public ParticleSystem flammethrower;
    public ParticleSystem rescueFX;
    public MMFeedbacks jumpFeedback;
    public MMFeedbacks superJumpFeedback;
    public MMFeedbacks fallFeedback;
    public MMFeedbacks jumpPerfectSpotFeedback;
    public MMFeedbacks InFireModeFeedback;
    public MMFeedbacks OutFireModeFeedback;
    public TextMeshProUGUI playerPositionGamePlayUI;
    private Outlinable outlinable;
    public RagdollControlleur ragdollControlleur;
    public SpriteRenderer circleFireMode;



    private Sequence jumpSequence; 
    private Tween jump;
    private Tween fall;
    private float startY;



    public virtual void Awake() {
        if(targetPosition)
            targetPosition.parent = null;
        
        startY = transform.position.y;
        isSuperJump = false;

        originalSpeedForwad = speedForward;
        originaljumpHeight = jumpHeight;
        originalDurationMovement = durationMovement;
        originalScale = transform.lossyScale;
        originalDurationFallInWater = durationFallInWater;


        outlinable = GetComponent<Outlinable>();
        if(outlinable)
            outlinable.enabled = false;

        if(transform.tag.Equals("Player")){
            isPlayer = true;

            ragdollControlleur = GetComponentInChildren<RagdollControlleur>();
            ragdollControlleur.ToogleRagdool(false);
        }else{
            isPlayer = false;
        }
        
    }


    public virtual void InitPlatformDetection() {
        Platforme[] allPlatformArray = GameControlleur.Instance.GetAllPlatform();
        foreach (var platform in allPlatformArray)
        {
            allPlatform.Add(platform);
        }
    }

    public virtual void Start() {
        InitPlatformDetection();
        UpdateSignFireMode();
    }

    public virtual void StartMoving()
    {
        canMove = true;
        MoveAndJump();
    }

    void MoveAndJump(){
        if(canMove){

            jumpSequence = DOTween.Sequence();

            //Jump part
            jumpSequence.Append(transform.DOMoveZ(speedForward / 2, durationMovement / 2).SetRelative(true).SetEase(Ease.Linear)
            .OnStart(() =>{
                jump = pogoStick.DOMoveY(jumpHeight, durationMovement / 2).SetRelative(true).SetEase(Ease.OutQuad);
            })
            .OnComplete(() =>{
                jump.Kill(true);
            }));

            //Fall part
            jumpSequence.Append(transform.DOMoveZ(speedForward / 2, durationMovement / 2).SetRelative(true).SetEase(Ease.Linear)
            .OnStart(() =>{
                fall = pogoStick.DOMoveY(startY, durationMovement / 2).SetEase(Ease.InQuad);
            })
            .OnComplete(() =>{
                fall.Kill(true);
                CheckIfGronded();
                MoveAndJump();
            }));
            
        }

    }

    public virtual void CheckIfGronded(){
        RaycastHit raycastHit;
        
        //SphereCast
        // DebugPlus.DrawSphere(transform.position.SetY(1), radiusSphereCastDetectionPlatform).Duration(100f).Color(Color.red);
        bool hitPlatform = Physics.SphereCast(transform.position.SetY(startY + 2),radiusSphereCastDetectionPlatform ,Vector3.down, out raycastHit, 3, layerPlatform);

        bool justLandWithSuperJump = false;
        if(hitPlatform == false){
            FallOnWater();
        }else{
            if(raycastHit.transform.GetComponent<FinishLine>()){
                if(isPlayer && positionInTheRace == 1){
                    // Try the finisher platform
                    isOnFinisher = true;
                    if(isOnFireMode)
                        ToogleFireMode(false);
                }else{
                    Win(0); // If not a player and not first, this is the end
                }
            }

            
            // Atterisage du SuperJump
            if(isSuperJump && raycastHit.transform.GetComponentInParent<Platforme>()){
                raycastHit.transform.GetComponentInParent<Platforme>().KillOtherPlayerOnPlatforme(this);
                raycastHit.transform.GetComponentInParent<Platforme>().LandWithSuperJump();
                justLandWithSuperJump = true;
            }

            if(raycastHit.transform.tag.Equals("Trampoline")){
                if(transform.tag.Equals("Player"))
                    superJumpFeedback.PlayFeedbacks();
                raycastHit.transform.GetComponent<DOTweenAnimation>().DOPlay();

                if(isGiant){
                    GiantJump();
                }else{
                    SuperJump();
                }

            }else{
                // Déclenche le feedback pour le joueur et pour les IA si les platform sont en mode destruction
                if(GameControlleur.Instance.destructivePlatform && justLandWithSuperJump == false){
                    raycastHit.transform.GetComponent<Platforme>().JumpOnPlatform();
                }else{
                    if(transform.tag.Equals("Player") && raycastHit.transform.tag.Equals("PerfectSpot")){
                        raycastHit.transform.GetComponentInParent<Platforme>().JumpOnPerfectSpot();
                        if(transform.tag.Equals("Player"))
                                jumpPerfectSpotFeedback.PlayFeedbacks();
                        GameControlleur.Instance.FadeOutAlphaSpot(false);
                        if(isOnFireMode == false)
                            numberOfChainPerfectSpot += 1;

                    }else{
                        // Déclenche le feedback uniquement pour le joueur si les platform sont en mode normal
                        if(transform.tag.Equals("Player") && justLandWithSuperJump == false)
                            raycastHit.transform.GetComponent<Platforme>().JumpOnPlatform();
                            if(transform.tag.Equals("Player"))
                                jumpFeedback.PlayFeedbacks();
                            if(isOnFireMode == false)
                                numberOfChainPerfectSpot = 0;

                    }
                }

                if(isGiant){
                    GiantJump();
                }else{
                    NormalJump();
                }

                if(isOnFinisher && raycastHit.transform.tag.Equals("LastPlatformFinisher")){
                    Win(15); // Atteris sur la dernier platform du finish
                    return;
                }



                if(isOnFireMode){
                    tmp_platformJumpOnFireMode++;
                    if(tmp_platformJumpOnFireMode >= numberOfPlatformToJump){
                        ToogleFireMode(false);
                        return;
                    }
                    DetectNextPlatform();
                }

                if(numberOfChainPerfectSpot >= numberOfChainPerfectSpotToActivateFireMode){
                    ToogleFireMode(true);
                    DetectNextPlatform();
                }

                UpdateSignFireMode();
            }
        }
    }

    public virtual void DetectNextPlatform(){

        List<Platforme> availablePlatforme = new List<Platforme>();

        // Ne récupere que les plaformes qui sont sur la ligne suivante
        for (int i = 0; i < allPlatform.Count; i++)
        {
            bool platformIsOnTheNextRow = (Mathf.Round(allPlatform[i].transform.position.z) == Mathf.Round(transform.position.z + speedForward));
            if(platformIsOnTheNextRow && allPlatform[i].isDestroy == false){
                availablePlatforme.Add(allPlatform[i]);
            }    
        }
        
        // Choisis la platforme la plus proche de l'IA
        Platforme nextPlatform = GetCloserPlatforme(availablePlatforme);

        // Si la platforme est un trampoline, on evite de le prendre
        if(nextPlatform.isTrampoline){
            availablePlatforme.Remove(nextPlatform);
            nextPlatform = GetCloserPlatforme(availablePlatforme);
        }

        MovePlayerToNextPlatform(nextPlatform.transform);   
    }

    public void MovePlayerToNextPlatform(Transform platform){
        if(canMove && isOnFireMode){
            transform.DOMoveX(platform.position.x, durationMovement);
            targetPosition.DOMoveX(platform.position.x, durationMovement);
        }
    }

    private void UpdateSignFireMode(){
        if(circleFireMode){
            float remapPlatformJump = 0;
            if(isOnFireMode){
                // Let value to 0 = full filling
            }else{
                remapPlatformJump = MathFunction.Remap((float) numberOfChainPerfectSpot, 0f, numberOfChainPerfectSpotToActivateFireMode, 360f, 0f);
            }
            circleFireMode.material.DOFloat(remapPlatformJump, "_Arc1", durationMovementDuringFireMode);
        }
    }

    public void ToogleFireMode(bool state){
        if(state == true){
            isOnFireMode = true;
            numberOfChainPerfectSpot = 0;
            tmp_platformJumpOnFireMode = 0;
            durationMovement = durationMovementDuringFireMode;
            InFireModeFeedback.PlayFeedbacks();
            GameControlleur.Instance.ShowVignetteFireMode(true);
        }

        if(state == false){
            isOnFireMode = false;
            tmp_platformJumpOnFireMode = 0;
            jumpHeight = jumpHeightOutFireMode;
            durationMovement = durationMovementOutFireMode;
            OutFireModeFeedback.PlayFeedbacks();
            UpdateSignFireMode();
            GameControlleur.Instance.ShowVignetteFireMode(false);
        }
    }

    public void EatGiantPowerUp(){
        transform.DOScale(giantScale, 1f);
        tmpCooldownGiantPowerUp = 0f;
        durationFallInWater = durationFallInWater / 2f;
        GiantJump();
    }

    public void EndGiantPowerUp(){
        transform.DOScale(originalScale, 1f);
        durationFallInWater = originalDurationFallInWater;
        isGiant = false;
    }

    private void GiantJump(){
        isGiant = true;
        isSuperJump = true;
        speedForward = giantSpeedForward;
        jumpHeight = giantJumpheight;
        durationMovement = giantDurationMovement;
    }

    private void CooldownGiant(){
        tmpCooldownGiantPowerUp += Time.deltaTime;

        if(tmpCooldownGiantPowerUp > durationGiantPowerUp){
            EndGiantPowerUp();
        }
    }

    private void SuperJump(){
        isSuperJump = true;
        speedForward = superSpeedForward;
        jumpHeight = superJumpHeight;
        durationMovement = superDurationMovement;
        flammethrower.Play();
        if(isPlayer)
            MMVibrationManager.ContinuousHaptic(0.25f, 0f, superDurationMovement, HapticTypes.None, this, this);
    }
    
    private void NormalJump(){
        isSuperJump = false;
        speedForward = originalSpeedForwad;
        jumpHeight = originaljumpHeight;
        durationMovement = originalDurationMovement;

        if(isOnFireMode){
            durationMovement = durationMovementDuringFireMode;
        }
        
        if(flammethrower.isPlaying)
            flammethrower.Stop();
    }

    public virtual void Win(int platformeMultiplicateur){
        canMove = false;
        jumpSequence.Kill();
        ResetRotation();
        NormalJump();
        GameControlleur.Instance.WinGame(platformeMultiplicateur);
    }

    public void ResetRotation(){
        perssonageParent.DORotate(Vector3.zero, 1f); // Reset Rotation
    }

    public virtual void FallOnWater(){
        numberOfChainPerfectSpot = 0;
        UpdateSignFireMode();
        canMove = false;
        fallFeedback.PlayFeedbacks();
        pogoStick.DOMoveY(-10f, durationFallInWater).SetRelative(true);
        ResetRotation();
        jumpSequence.Pause();

        if(GameControlleur.Instance.rescuePlayerWhenTheyFall){
            NormalJump();
            StartCoroutine(PutThePlayerBackOnTheRace());
        }else{
            Lose();
        }
    }

    public virtual IEnumerator PutThePlayerBackOnTheRace(){
        yield return new WaitForSeconds(durationFallInWater);

        rescueFX.Play();

        

        List<Platforme> platformOnSameRow = new List<Platforme>();

        Platforme[] allPlatformArray = GameControlleur.Instance.GetAllPlatform();
        if(isOnFinisher){
            // Recupere les platformes qui sont sur la lignes d'avant
            for (int i = 0; i < allPlatformArray.Length; i++)
            {
                if(Mathf.Round(allPlatformArray[i].transform.position.z) == Mathf.Round(transform.position.z - 5f)){
                    platformOnSameRow.Add(allPlatformArray[i]);
                }    
            }
            durationFallInWater = 0.15f;
        }else{

            // Ne récupere que les plaformes qui sont sur la ligne actuelle
            for (int i = 0; i < allPlatformArray.Length; i++)
            {
                if(Mathf.Round(allPlatformArray[i].transform.position.z) == Mathf.Round(transform.position.z)){
                    platformOnSameRow.Add(allPlatformArray[i]);
                }    
            }
        }

        Platforme closerPlatform = GetCloserPlatforme(platformOnSameRow);

        if(closerPlatform.movingParent){
            closerPlatform.movingParent.StopMoving();
        }

        pogoStick.DOMoveY(startY, durationFallInWater).OnComplete(() =>{
            if(targetPosition)
                targetPosition.DOMove(closerPlatform.transform.position.SetY(startY), durationFallInWater);
            
            transform.DOMove(closerPlatform.transform.position.SetY(startY), durationFallInWater).OnComplete(() =>{
                canMove = true;
                if(isOnFinisher){
                    Win(closerPlatform.multiplicateurValue);
                }else{
                    CheckIfGronded();
                }

                if(canMove) // Ne bouge pas si le joueurs a gagné
                    MoveAndJump();
                
                rescueFX.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
                
            });
        });
    }

    public Platforme GetCloserPlatforme(List<Platforme> listPlatforme){

        float shorterDistance = float.MaxValue;
        Platforme nextPlatform = null;

        for (int i = 0; i < listPlatforme.Count; i++)
        {
            float distance = Vector3.Distance(transform.position, listPlatforme[i].transform.position);
            if(distance < shorterDistance){
                shorterDistance = distance;
                nextPlatform = listPlatforme[i];
            }
        }
        return nextPlatform;
    }

    public virtual void Lose(){
        canMove = false;
        GameControlleur.Instance.LoseGame();
        
    }

    public void DieFromSuperJump(){
        jumpSequence.Kill(false);
        killed = true;

        if(ragdollControlleur){
            ragdollControlleur.transform.parent = null;
            ragdollControlleur.ToogleRagdool(true);
            Vector3 direction = (Vector3.up * forceProjectionWhenKilled) + (Vector3.Lerp(Vector3.right, Vector3.left, Random.value) * forceProjectionWhenKilled * 2f);
            ragdollControlleur.AddForceToRagdool(direction);
            Destroy(ragdollControlleur.gameObject, 5f);
        }

        pogoStick.gameObject.SetActive(false); // Hide death player
        Lose();
    }

    public virtual void Update() {
        Vector2 directionInputPlayer = touchControlleur.GetInputFromLeanTouch();

        if(canMove){
            if(isOnFireMode == false){
                MovementOnXAxisPlayer(directionInputPlayer);
            }

            outlinable.enabled = true;
        }else{
            outlinable.enabled = false;
        }

        if(isGiant){
            CooldownGiant();
        }

        DisplayRacePosition();
    }

    public void DisplayRacePosition(){
        string prefix = null;
        switch(positionInTheRace){
            case 1:
                prefix = "st";
            break;

            case 2:
                prefix = "nd";
            break;

            case 3:
                prefix = "rd";
            break;

            default:
                prefix = "th";
            break;
        }
        racePositionText.text = positionInTheRace.ToString() + prefix;
        
        if(playerPositionGamePlayUI)
            playerPositionGamePlayUI.text = positionInTheRace.ToString() + prefix;
    }

    void MovementOnXAxisPlayer(Vector2 direction){
        Vector3 directionInput;
        
        // Custom Method
        //directionInput = new Vector3(touchControlleur.GetXAxis(), 0f, 0f);

        // Lean Touch Method
        directionInput = direction;


        
        // Move Target
        float stepInput = speedMovementInput * Time.deltaTime;
        Vector3 newInputPosition = Vector3.MoveTowards(Vector3.zero, directionInput, stepInput);
        targetPosition.Translate(newInputPosition);

        // Move Player
        transform.position = Vector3.Lerp(transform.position, transform.position.SetX(targetPosition.position.x), Time.deltaTime * speedMovementPlayer);

        RotateOnXAxisPlayer(directionInput.x);
    }

    void RotateOnXAxisPlayer(float Xdirection){        
        float rotationT = 0.5f + (Xdirection * speedRotationInput);
        float newRotationZ = Mathf.Lerp(maxAngle, minAngle, rotationT);
        Vector3 rotationPlayer = new Vector3(0f, 0f, newRotationZ);

        perssonageParent.rotation = Quaternion.Lerp(perssonageParent.rotation, Quaternion.Euler(rotationPlayer), Time.deltaTime * speedRotationPlayer);
    }

    public float GetStartYPosition(){
        return startY;
    }

    public float GetZPositon(){
        return transform.position.z;
    }

}
