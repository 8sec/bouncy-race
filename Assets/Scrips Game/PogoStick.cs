﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

public class PogoStick : MonoBehaviour
{
    [ReadOnly] [SerializeField] public bool isJumping = false;
[Title("Jump")]
    [SerializeField] public float jumpHeight = 5f;
    [SerializeField] public float durationJump = 5f;
    [SerializeField] public Ease jumpEase;
    [SerializeField] private bool useCustomJumpCurve = false;
    [ShowIf("useCustomJumpCurve")] [SerializeField] private AnimationCurve customJumpEase;


[Title("Fall")]
    [SerializeField] public float durationFall = 5f;
    [SerializeField] public Ease fallEase;
    [SerializeField] private bool useCustomFallCurve = false;
    [ShowIf("useCustomFallCurve")] [SerializeField] private AnimationCurve customFallEase;

    
}
