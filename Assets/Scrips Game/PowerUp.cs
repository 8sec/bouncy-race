﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        PlayerMovement runner = other.GetComponent<PlayerMovement>();
        runner.EatGiantPowerUp();
        Destroy(gameObject);
    }
}
