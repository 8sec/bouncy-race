﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RacePosition : MonoBehaviour
{
    public bool showIAProgression = false;
    public RectTransform prefabIAPosition;
    public RectTransform parentIAPosition;
    public RectTransform playerPosition;
    public Image slider;
    public RectTransform startPos;
    public RectTransform endPos;
    public List<RectTransform> listIAPosition = new List<RectTransform>();
    public List<ArtificialIntelligence> listIA = new List<ArtificialIntelligence>();


    // Start is called before the first frame update
    void Start()
    {
        SetLevelIndicator();
        if(showIAProgression)
            InstanciateIAPosition();
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePlayerPosition();
        if(showIAProgression)
            UpdateIAPosition();
    }

    private void InstanciateIAPosition(){
        foreach (ArtificialIntelligence IA in GameControlleur.Instance.GetAllIAs())
        {
            RectTransform IARacePosition = Instantiate(prefabIAPosition, parentIAPosition);
            listIAPosition.Add(IARacePosition);
            listIA.Add(IA);
        }
    }

    private void SetLevelIndicator(){
        int index = LevelManager.Instance.GetLevelIndex();
        startPos.GetComponentInChildren<TextMeshProUGUI>().text = (index + 1).ToString();
        endPos.GetComponentInChildren<TextMeshProUGUI>().text = (index + 2).ToString();
    }

    private void UpdatePlayerPosition(){
        bool isAlive = !GameControlleur.Instance.playerMovement.killed;

        playerPosition.GetComponent<IAPositionUI>().ShowStateAlive(isAlive);
        playerPosition.position = Vector3.Lerp(startPos.position, endPos.position, slider.fillAmount);
    }

    private void UpdateIAPosition(){

        for (int i = 0; i < listIAPosition.Count; i++)
        {
            // State
            bool isAlive = !listIA[i].killed;
            listIAPosition[i].GetComponent<IAPositionUI>().ShowStateAlive(isAlive);

            // Position
            float t = Mathf.InverseLerp(GameControlleur.Instance.playerMovement.GetStartYPosition(), GameControlleur.Instance.GetFinishLine().transform.position.z, listIA[i].GetZPositon());
            listIAPosition[i].position = Vector3.Lerp(startPos.position, endPos.position, t);
        }
    }
}
