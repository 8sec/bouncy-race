﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

public class ThemeManager : MonoBehaviourSingleton<ThemeManager>
{
    public List<Material> platformMaterial;
    public List<Material> waterMaterial;
    public List<GameObject> perssonageList;
    public MeshRenderer water;
    public Camera mainCamera;

    public bool forceTheme = false;

    [ShowIf("forceTheme")]
    public int forceThemeIndex = 0;

    public int GetThemeIndexForRoadMap(){
        int indexTheme;
        if(forceTheme){
            indexTheme = forceThemeIndex - 1;
        }else{
            float themeIndexRAW = ((float)(LevelManager.Instance.CurrentLevelIndex) / 5f);
            indexTheme = Mathf.FloorToInt(themeIndexRAW % platformMaterial.Count);
        }

        return indexTheme;
    }

    public void ApplyTheme(){
        int themeIndex = GetThemeIndexForRoadMap();

        // Setup water
        water.material = waterMaterial[themeIndex];

        // Setup Camera
        mainCamera.backgroundColor = waterMaterial[themeIndex].GetColor("_DepthColor2");

        // Setup Platforme
        Platforme[] allPlatform = GameControlleur.Instance.GetAllPlatform();
        foreach (Platforme platforme in allPlatform)
        {
            platforme.GetComponent<MeshRenderer>().material = platformMaterial[themeIndex];
        }

        // Setup Trampoline
        Trampoline[] allTrampoline = FindObjectsOfType<Trampoline>();
        foreach (Trampoline trampoline in allTrampoline)
        {
            trampoline.chevron.GetComponent<MeshRenderer>().material = platformMaterial[themeIndex];
        }

        // Setup Fog
        RenderSettings.fog = true;
        RenderSettings.fogColor = waterMaterial[themeIndex].GetColor("_DepthColor2");
        RenderSettings.fogMode = FogMode.ExponentialSquared;
        RenderSettings.fogDensity = 0.01f;
    }

    public GameObject GetRandomCharacter(){
        int rand = Random.Range(0, perssonageList.Count);
        return perssonageList[rand];
    }
}
